import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Dump Trump");

        Canvas canvas = new Canvas(scene.getWidth(), scene.getHeight());
        root.getChildren().add(canvas);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        Spielfeld spielfeld = new Spielfeld(gc);
        InputHandler inputHandler = new InputHandler(spielfeld);
        scene.setOnMouseClicked(
                mouseEvent -> inputHandler.pruefeKlick((int) mouseEvent.getX(), (int) mouseEvent.getY())
        );

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

