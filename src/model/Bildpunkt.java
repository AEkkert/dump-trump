package model;

import java.util.Random;

public class Bildpunkt {

    private Random random = new Random();

    private int x;
    private int y;
    private int length = 50;

    public Bildpunkt() {
        this.x = getRandomCoord();
        this.y = getRandomCoord();
    }

    private int getRandomCoord() {
        return random.nextInt((300-100)+1)+100;
    }

    public int getLength() {
        return length;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
