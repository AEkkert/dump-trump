import model.Bildpunkt;

public class InputHandler {

    private Spielfeld spielfeld;

    public InputHandler(Spielfeld spielfeld) {
        this.spielfeld = spielfeld;
    }

    public void pruefeKlick(int x, int y) {
        // Position Maus x/y vergleichen mit Position Kopf)
        Bildpunkt punkt = spielfeld.getBildpunkt();
        if (x > (punkt.getX() - punkt.getLength() /2) && x < (punkt.getX() + punkt.getLength() /2) &&
                y > (punkt.getY() - punkt.getLength() /2) && y < (punkt.getY() + punkt.getLength() /2)) {
            spielfeld.punktLoeschen();
            spielfeld.punktErstellen();
        }
    }

}
