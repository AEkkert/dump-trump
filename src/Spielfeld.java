import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import model.Bildpunkt;

public class Spielfeld {

    private Bildpunkt bildpunkt;
    private GraphicsContext gc;

    public Spielfeld(GraphicsContext gc) {
        this.gc = gc;
        gc.setFill(Color.RED);
        this.punktErstellen();
    }

    public void punktErstellen() {
        //zeichne den kopf an random pos
        this.bildpunkt = new Bildpunkt();
        int length = this.bildpunkt.getLength();
        // Quadrat ohne Trump
        //gc.fillRect(this.bildpunkt.getX() - length /2, this.bildpunkt.getY() - length /2, length, length);
        String imagePath = "uncleTrump.jpg";
        Image image = new Image(imagePath);
        gc.drawImage(image, this.bildpunkt.getX() - length / 2, this.bildpunkt.getY() - length / 2, length, length);
    }


    public void punktLoeschen() {
        gc.clearRect(0, 0, 400, 400);
    }

    public Bildpunkt getBildpunkt() {
        return bildpunkt;
    }

}
